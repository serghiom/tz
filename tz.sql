-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 17 2017 г., 08:31
-- Версия сервера: 5.6.33-0ubuntu0.14.04.1
-- Версия PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `dealer`
--

CREATE TABLE `dealer` (
  `id` int(11) NOT NULL,
  `dealer_name` varchar(150) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dealer`
--

INSERT INTO `dealer` (`id`, `dealer_name`, `address`, `phone`) VALUES
(1, '1-й диллер ', 'Маяковская 10', '0931111111'),
(2, '2-й диллер ', 'Пушкинская15', '0932222222'),
(3, '3-й диллер', 'Цветной бульвар 30', '0933333333'),
(4, '4-й диллер', 'Маяковская 45', '0934444444');

-- --------------------------------------------------------

--
-- Структура таблицы `metro_dealer`
--

CREATE TABLE `metro_dealer` (
  `metro_station_id` int(11) NOT NULL DEFAULT '0',
  `dealer_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `metro_dealer`
--

INSERT INTO `metro_dealer` (`metro_station_id`, `dealer_id`) VALUES
(2, 1),
(2, 2),
(3, 4),
(4, 1),
(4, 2),
(4, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `metro_station`
--

CREATE TABLE `metro_station` (
  `id` int(11) NOT NULL,
  `metro_station_name` varchar(150) DEFAULT NULL,
  `metro_station_coords` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `metro_station`
--

INSERT INTO `metro_station` (`id`, `metro_station_name`, `metro_station_coords`) VALUES
(1, 'Белорусская', '255:320'),
(2, 'Маяковская', '295:360'),
(3, 'Пушкинская', '345:415'),
(4, 'Цветной бульвар', '400:337');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `dealer`
--
ALTER TABLE `dealer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `metro_dealer`
--
ALTER TABLE `metro_dealer`
  ADD PRIMARY KEY (`metro_station_id`,`dealer_id`);

--
-- Индексы таблицы `metro_station`
--
ALTER TABLE `metro_station`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `dealer`
--
ALTER TABLE `dealer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `metro_station`
--
ALTER TABLE `metro_station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
