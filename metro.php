<?php

//выборка всех станиции через ID
$sql = 'SELECT * FROM metro_station WHERE id = :id';
$sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$sth->execute(array(':id' => $_GET['id']));
$metro = $sth->fetch();

//выборка всех диллеров связаных со станций через ID станции
$sql = 'SELECT dealer.* FROM dealer '
        . ' left join metro_dealer on dealer.id  = metro_dealer.dealer_id '
        . ' WHERE metro_dealer.metro_station_id = :id ';
$sth = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$sth->execute(array(':id' => $_GET['id']));
$dealers = $sth->fetchAll();


if(!$metro){ 
    echo '<h1>404</h1>'; die;
}


?>
<a href="/">На главную</a>
<h1><?=$metro['metro_station_name']?></h1>


<div id="table">
    <table >
        <thead>
            <tr>    
                <td class="station">Название</td>
                <td class="station">Адрес</td>
                <td class="station">Телефон</td>
            </tr>    
        </thead>
        <tbody>
<?php foreach ($dealers as $dealer) { //перебор диллеров ?>
                <tr>
                    <td class="station"><?= $dealer['dealer_name'] ?></td>
                    <td class="station"><?= $dealer['address'] ?></td>
                    <td class="station"><?= $dealer['phone'] ?></td>
                </tr>
<?php } ?>                    
        </tbody> 
    </table>    
</div>