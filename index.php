<?php
error_reporting(E_ALL);
require_once 'config.php';

$content = 'main.php';
$base_url = "http://" . $_SERVER['SERVER_NAME'];
if (isset($_GET['q'])) {
    if (preg_match("/.html$/", $_GET['q'])) {
        $_GET['q'] = substr($_GET['q'],0,-5);
    }
    $params = explode("/", $_GET['q']);
    $_GET['conntroller'] = $params[0];
    if(isset($params[1])){
        $_GET['id'] = $params[1];
    }    
}

if (isset($_GET['conntroller'])) {
    if ($_GET['conntroller']) {
        $content = $_GET['conntroller'].'.php';
    }
}
?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="<?=$base_url?>/style.css"/>
    </head>
    <body>
        <div id="content">
            <?php  require_once $content; ?>
        </div>    
    </body>
</html>