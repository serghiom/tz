<?php 
    $metro_list = $pdo->query('SELECT id, metro_station_name, metro_station_coords, count(metro_dealer.dealer_id) as count_dealer FROM metro_station'
            . ' left join metro_dealer on metro_dealer.metro_station_id  = metro_station.id where 1 group by metro_station.id')->fetchAll(PDO::FETCH_ASSOC);
    //выборка станций метро с БД с таблиц metro_station, с metro_dealer. Через таблицу metro_dealer счетаем к-во диллеров на станции

?>

<div id="map">
    <?php foreach ($metro_list as $metro) {  // перебор спомощью foreach станций метро. Размещаются активные блоки на карте?>
        <?php $coords = explode(':', $metro['metro_station_coords']) ?>
        <a href="/metro/<?= $metro['id'] ?>.html" class="metro_station" title="<?= $metro['metro_station_name'] ?>" style="left: <?= $coords[0] ?>px; top: <?= $coords[1] ?>px;"></a>
    <?php } ?>

</div>
<div id="bar">

    <div id="table">
        <table >
        <?php foreach ($metro_list as $metro) { // перебор спомощью foreach станций метро. Формирует Таблицу с к-вом диллеров ?>
            <tr>
            <td class="station"><?= $metro['metro_station_name'] ?></td>
            <td class="station"><?= $metro['count_dealer'] ?></td>
            </tr>
        <?php } ?>                    
        </table>    
    </div>
    <div id="metro_station">
        <!-- Формирование листа со всемми станциями, с переходом на страницу станции -->
        <select name="metro_station" onchange="selectStatio(this)">
            <option value="">Все станции</option>
            <?php foreach ($metro_list as $metro) { ?>
                <option value="<?= $metro['id'] ?>"><?= $metro['metro_station_name'] ?></option>
            <?php } ?>
        </select>
    </div>    
    <div id="metro_dealer">
        <a href="/dealers.html">Полный список диллеро</a>

    </div>                 
</div>
<script>
    
    /* метод для перехода на страницу станции*/
    function selectStatio(obj){
        var id = parseInt(obj.value);
        if(id > 0){
            location.href ='/metro/'+id+'.html';
        }
    }
</script>
