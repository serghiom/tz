<?php
//выборка всех диллеров с к-вом связаных с ним станция
$dealer_list = $pdo->query('SELECT dealer.*, count(metro_dealer.metro_station_id) as count_metro_station FROM dealer '
                . ' left join metro_dealer on dealer.id  = metro_dealer.dealer_id where 1 group by dealer.id')->fetchAll(PDO::FETCH_ASSOC);
?>
<a href="/">На главную</a>
<div id="table">
    <table >
        <thead>
            <tr>    
                <td class="station">Название</td>
                <td class="station">Адрес</td>
                <td class="station">Телефон</td>
                <td class="station">Станций</td>                
            </tr>    
        </thead>
        <tbody>
<?php foreach ($dealer_list as $dealer) { //формирование таблицы всех диллеров ?>
                <tr>
                    <td class="station"><?= $dealer['dealer_name'] ?></td>
                    <td class="station"><?= $dealer['address'] ?></td>
                    <td class="station"><?= $dealer['phone'] ?></td>
                    <td class="station"><?= $dealer['count_metro_station'] ?></td>
                </tr>
<?php } ?>                    
        </tbody> 
    </table>    
</div>